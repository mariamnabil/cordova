import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-folder',
  templateUrl: './folder.page.html',
  styleUrls: ['./folder.page.scss'],
})
export class FolderPage implements OnInit {
   cordova: any; 
   ckoCardTokenRequest = {
    number: "4543474002249996",
    expiry_month: 6,
    expiry_year: 2025,
    name: "Bruce Wayne",
    cvv: "956",
    billing_address: {
        address_line1: "Checkout.com",
        address_line2: "90 Tottenham Court Road",
        city: "London",
        state: "London",
        zip: "W1T 4TJ",
        country: "GB"
    },
    phone: {
        country_code: "+1",
        number: "4155552671"
    }
};
getToken(){

  this.cordova.plugins.Checkout.initSandboxClient("pk_test_b35f8983-91bb-43f8-80df-417945fc6a4d", 
  function() {
       console.log("Success, no need to do anything");
  }, function (error) {
    console.log("error");
  });


function onSuccess(tokenResponse) {
  alert(tokenResponse)
    console.log('Tokenization successful', tokenResponse);
}

function onError(errorMessage) {
    console.log('Error generating token', errorMessage);
    alert(errorMessage)

}
let response: any;
response = this.cordova.plugins.Checkout.generateToken(this.ckoCardTokenRequest, onSuccess, onError);
// console.log(response);

}
  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.getToken()
  }
}
